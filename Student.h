#include <string>
#include <vector>

class Student
{
public:
	std::string name;
	std::string degree;
	std::vector<int> * getGroups();
	Student(std::string name, std::string degree);
	void addGroup(int assignment, int groupnumber);
private:
	std::vector<int> groups;

};