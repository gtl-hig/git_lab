#include <stdio.h>
#include <vector>
#include <string>
#include "Student.h"


//! Main
/*!
 *	A small program that prints strings from a vector.
 */
int main(int argc, char const *argv[])
{
	std::vector<std::string> strings = {
		"Hello World!",
		"Hello Abby!"
	};
	strings.push_back("Johannes Hovland");
	strings.push_back("Simon McCallum");
	// Each student should alter a line below to include their name in the printout
	strings.push_back("Jane Doe");
	strings.push_back("Martin Sandberg");
    strings.push_back("Kristoffer Baardseth");
	strings.push_back("Abatare Shabani");
	strings.push_back("Asle Vestly Andersen");
   	strings.push_back("Glenn Adrian Nordhus");
   	strings.push_back("Benjamin Gordon Wendling");
	strings.push_back("Hakan Shehu");
	strings.push_back("Adam Jammary");
	strings.push_back("Magnus Enggrav");
	strings.push_back("Kristoffer Baardseth");
	strings.push_back("Oystein Kjevik");
	strings.push_back("Asle Vestly Andersen");
   	strings.push_back("Glenn Adrian Nordhus");
   	strings.push_back("Odd-Kjetil Aamot Dahl");
   	strings.push_back("Marius Stubberud");
	strings.push_back("Joakim Smikkerud");
	strings.push_back("H�konpus");
   	strings.push_back("Dag Kristian Lode Gjerberg"
	strings.push_back("Hans Emil Eid");	
	strings.push_back("Joakim Smikkerud");
	strings.push_back("Morten Bjerke");
	strings.push_back("Christoffer Kongsness");
	strings.push_back("Joakim Lebesby");
	strings.push_back("Tony Andr� Furuhaug Kolstad");
	strings.push_back("Jonas Dalheim Reitan");
	strings.push_back("Martin Bjerknes");
	// You have gone too far. Add your name above.
	for (auto s : strings) {
		printf("%s\n", s.c_str());
	}

	return 0;
}
